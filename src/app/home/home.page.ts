import { Component, ViewEncapsulation } from '@angular/core';
import { DataService } from '../services/data.service';
import * as moment from 'moment';
import { Flight } from '../models/flight';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomePage {
  public flightType = 'A';  // versus 'D' for departures
  public flightSearchString: string;
  public allFlights: Array<Flight> = [];

  constructor(private data: DataService) {
    // Start with retrieving the default data right away
    this.refreshData();
  }

  public refreshData(): void {
    // Default start date is now
    const startTime: string = this._convertISOString(moment().toISOString());
    // End of the timeline is tomorrow, so we will use end of day tomorrow
    const endTime = this._convertISOString(moment().add(1, 'days').endOf('day').toISOString());

    this.data.getFlightData(startTime, endTime, this.flightType, navigator.language, this.flightSearchString)
      .subscribe((result: any) => {
          this.allFlights = [];
          for (const aFlight of result.operationalFlights) {
           if (aFlight.flightNumber.toString() === this.flightSearchString || !this.flightSearchString) {
             const aNewFlight: Flight = new Flight(
               aFlight.flightNumber,
               this.getScheduledAndLatestTime(aFlight).scheduled,
               this.getScheduledAndLatestTime(aFlight).latest,
               aFlight.flightStatusPublicLangTransl,
               this.flightType);
             this.allFlights.push(aNewFlight);
           }

           this.allFlights.sort((a, b) => {
             if (moment(a.getScheduledTime()).isBefore(b.getScheduledTime())) {
               return -1;
             }
             if (moment(a.getScheduledTime()).isAfter(b.getScheduledTime())) {
               return 1;
             }
             return 0;
           });
        }
    });
  }

  private _convertISOString(aDateString: string): string {
    return aDateString.substr(0, aDateString.length - 5) + 'Z';
  }

  public toggleFlightType(): void {
    this.flightType = this.flightType === 'A' ? 'D' : 'A';

    this.refreshData();
  }

  private _getRightLeg(allFlightLegs: Array<any>): any {
    for (const aLeg of allFlightLegs) {
       const airport = this.flightType === 'A' ? aLeg.arrivalInformation.airport.code : aLeg.departureInformation.airport.code;

       if (airport === 'AMS') {
         return aLeg;
       }
    }
  }

  public getScheduledAndLatestTime(aFlight: any): { scheduled: any, latest: any } {
    if (!aFlight) {
      return { scheduled: undefined, latest: undefined };
    }

    // first find the leg with AMS as the airport
    const rightLeg: any = this._getRightLeg(aFlight.flightLegs);

    if (!rightLeg) {
      return { scheduled: undefined, latest: undefined };
    }

    if (this.flightType === 'A') {
      return {
        scheduled: moment(rightLeg.arrivalInformation.times.scheduled).format('DD-MM HH:mm'),
        latest: moment(rightLeg.arrivalInformation.times.latestPublished).format('DD-MM HH:mm')
      };
    } else {
      return {
        scheduled: moment(rightLeg.departureInformation.times.scheduled).format('DD-MM HH:mm'),
        latest: moment(rightLeg.departureInformation.times.latestPublished).format('DD-MM HH:mm')
      };
    }
  }
}
