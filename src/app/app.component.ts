import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translate: TranslateService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.translateConfig();
    });
  }

  private translateConfig(): void {
    let userLang: string = navigator.language.split('-')[0];

    if (userLang !== 'nl' && userLang !== 'en') {
      userLang = 'en';
    }

    this.translate.use(userLang)
      .subscribe(
        () => { },
        err => { console.error(err); }
      );
  }
}
