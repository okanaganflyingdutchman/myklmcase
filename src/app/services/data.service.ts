import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  public getFlightData(startTime: any,
                       endTime: any,
                       flightType: string,
                       language: string,
                       flightNumber: string): Observable<any> {
    const headers = new HttpHeaders()
      .set('accept', 'application/hal+json;version=com.afkl.operationalflight.v3')
      .set('accept-language', language)
      .set('api-key', '96apartrc7tsbhhcymcpar5c');

    // tslint:disable-next-line:max-line-length
    const requestUrl = `https://api.airfranceklm.com/opendata/flightstatus?movementType=${flightType}&startRange=${startTime}&endRange=${endTime}&origin=AMS&destination=AMS`;

    return this.http.get(requestUrl, {headers});
  }
}
