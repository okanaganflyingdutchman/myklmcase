'use strict';

export class Flight {

  private flightNumber: number;
  private scheduledTime: any;
  private latestTime: any;
  private status: string;
  private type: string;

  constructor(flightNumber: number,
              scheduledTime: any,
              latestTime: any,
              status: string,
              type: string) {
    this.flightNumber = flightNumber;
    this.scheduledTime = scheduledTime;
    this.latestTime = latestTime;
    this.status = status;
    this.type = type;
  }

  public getScheduledTime(): string {
    return this.scheduledTime;
  }
}
